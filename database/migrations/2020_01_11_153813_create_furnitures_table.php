<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFurnituresTable extends Migration {

    public function up() {
        Schema::create('furnitures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug_name');
            $table->string('name');
            $table->integer('price');
            $table->string('desc')->nullable();
            $table->integer('long')->nullable();
            $table->integer('height')->nullable();
            $table->integer('width')->nullable();
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('furnitures');
    }

}
