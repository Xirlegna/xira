<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Furniture;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Furniture::class, function (Faker $faker) {
    $name = $faker->name;
    return [
        'slug_name' => Str::slug($name, '-'),
        'name' => $name,
        'price' => $faker->randomDigitNotNull,
        'desc' => $faker->text,
        'long' => $faker->randomDigitNotNull,
        'height' => $faker->randomDigitNotNull,
        'width' => $faker->randomDigitNotNull,
    ];
});
