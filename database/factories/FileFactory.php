<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\File;
use App\Furniture;
use Faker\Generator as Faker;

$factory->define(File::class, function (Faker $faker) {
    return [
        'product_id' => factory(Furniture::class),
        'product' => 'furnitures',
        'name' => $faker->regexify('[A-Za-z0-9]{20}'),
        'type' => $faker->fileExtension,
    ];
});
