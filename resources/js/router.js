import Vue from 'vue';
import VueRouter from 'vue-router';

import FurnituresIndex from './views/FurnituresIndex';
import FurnituresCreate from './views/FurnituresCreate';
import FurnituresShow from './views/FurnituresShow';
import FurnituresEdit from './views/FurnituresEdit';

import ContactsIndex from './views/ContactsIndex';
import ContactsCreate from './views/ContactsCreate';
import ContactsShow from './views/ContactsShow';
import ContactsEdit from './views/ContactsEdit';

import Logout from './actions/Logout';

Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {
            path: '/xira/furnitures',
            component: FurnituresIndex,
            name: 'furnituresIndex',
            meta: {title: 'Bútorok'}
        }, {
            path: '/xira/furnitures/create',
            component: FurnituresCreate,
            name: 'furnituresCreate',
            meta: {title: 'Új bútor'}
        }, {
            path: '/xira/furnitures/:id',
            component: FurnituresShow,
            name: 'furnituresShow',
            meta: {title: 'Bútor'}
        }, {
            path: '/xira/furnitures/:id/edit',
            component: FurnituresEdit,
            name: 'furnituresEdit',
            meta: {title: 'Bútor szerkesztése'}
        }, {
            path: '/xira/contacts',
            component: ContactsIndex,
            name: 'contactsIndex',
            meta: {title: 'Felhasználók'}
        }, {
            path: '/xira/contacts/create',
            component: ContactsCreate,
            name: 'contactsCreate',
            meta: {title: 'Új felhasználó'}
        }, {
            path: '/xira/contacts/:id',
            component: ContactsShow,
            name: 'contactsShow',
            meta: {title: 'Felhasználó'}
        }, {
            path: '/xira/contacts/:id/edit',
            component: ContactsEdit,
            name: 'contactsEdit',
            meta: {title: 'Felhasználó szerkesztése'}
        }, {
            path: '/xira/logout',
            component: Logout,
            name: 'logout'
        }
    ],
    mode: 'history'
});