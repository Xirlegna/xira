import Vue from 'vue';
import router from './router';
import App from './components/App';

require('./bootstrap');

Vue.prototype.$generateRandomString = function(length) {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (let i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
};

Vue.prototype.$slug = function(str) {
    str = str.replace(/^\s+|\s+$/g, '');
    str = str.toLowerCase();

    let from = "àáäâèéëêìíïîòóöőôùúüűûñç·/_,:;";
    let to = "aaaaeeeeiiiiooooouuuuunc------";
    for (let i = 0, l=from.length ; i < l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '').replace(/\s+/g, '-').replace(/-+/g, '-');

    return str;
};

Vue.prototype.$priceFormat = function(value) {
    let val = (value/1).toFixed().replace('.');
    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ') + ' Ft';
};

Vue.prototype.$addCm = function(value) {
    return value + ' cm'
}

const app = new Vue({
    el: '#app',
    components: {
        App
    },
    router
});