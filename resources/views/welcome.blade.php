@extends('layouts.app')

@section('title', 'XIRA | Home')

@section('content')
    <App :user="{{ auth()->user() }}"></App>
@endsection