@extends('layouts.app')

@section('content')
<div class="screen">
    <div class="auth">
        <div class="auth__content">
            <div class="auth__icon">XIRA</div>

            <h1 class="auth__welcome">Regisztáció</h1>
            <h2 class="auth__sub-welcome">Írja be a hitelesítő adatait</h2>

            <form method="POST" action="{{ route('register') }}">
                @csrf

                <div class="input-field">
                    <label for="name" class="input-field__label">Név</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="input-field__input" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                        @error('name')
                        <span class="alert alert--danger" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="input-field">
                    <label for="email" class="input-field__label">E-mail</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="input-field__input" name="email" value="{{ old('email') }}" required autocomplete="email">

                        @error('email')
                        <span class="alert alert--danger" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="input-field">
                    <label for="password" class="input-field__label">Jelszó</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="input-field__input" name="password" required autocomplete="new-password">

                        @error('password')
                        <span class="alert alert--danger" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="input-field">
                    <label for="password-confirm" class="input-field__label">Jelszó ismét</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="input-field__input" name="password_confirmation" required autocomplete="new-password">
                    </div>
                </div>

                <button type="submit" class="auth__btn btn btn--primary">Regisztáció</button>

            </form>
        </div>
    </div>
</div>
</div>
@endsection
