@extends('layouts.app')

@section('content')
<div class="screen">
    <div class="auth">
        <div class="auth__content">
            <div class="auth__icon">XIRA</div>

            <h1 class="auth__welcome">Bejelentkezés</h1>
            <h2 class="auth__sub-welcome">Írja be a hitelesítő adatait</h2>

            <form method="POST" action="{{ route('login') }}" class="pt-8">
                @csrf

                <div class="input-field">
                    <label for="email" class="input-field__label">E-mail</label>

                    <div>
                        <input id="email" type="email" class="input-field__input" name="email" value="{{ old('email') }}" autocomplete="email" autofocus placeholder="email@email.hu">

                        @error('email')
                        <span class="alert alert--danger" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="input-field">
                    <label for="password" class="input-field__label">Jelszó</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="input-field__input" name="password" autocomplete="current-password" placeholder="Jelszó">

                        @error('password')
                        <span class="alert alert--danger" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                
                <label class="checkbox-field">
                    Emlékezz rám
                    <input class="checkbox-field__input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    <span class="checkbox-field__checkmark"></span>
                </label>

                <button type="submit" class="auth__btn btn btn--primary">Belépés</button>
            </form>
        </div>
    </div>
</div>
@endsection
