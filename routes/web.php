<?php

Auth::routes();

Route::get('/logout-manual', function() {
    request()->session()->invalidate();
});

Route::get('/xira/{any}', 'XiraController@index')->where('any', '.*');

Route::get('/admin/user/roles', ['middleware' => 'role', function(){
    return 'Middleware Role';
}]);