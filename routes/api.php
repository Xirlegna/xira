<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->group(function () {
    Route::get('/contacts', 'ContactsController@index');
    Route::post('/contacts', 'ContactsController@store');
    Route::get('/contacts/{contact}', 'ContactsController@show');
    Route::patch('/contacts/{contact}', 'ContactsController@update');
    Route::delete('/contacts/{contact}', 'ContactsController@destroy');

    Route::get('/files/furniture/{furniture}', 'FilesController@indexFurniture');
    Route::post('/files', 'FilesController@store');
    Route::get('/files/{file}', 'FilesController@show');
    Route::patch('/files/{file}', 'FilesController@update');
    Route::delete('/files/{file}', 'FilesController@destroy');
    Route::post('/files/upload', 'FilesController@upload');

    Route::get('/furnitures', 'FurnituresController@index');
    Route::post('/furnitures', 'FurnituresController@store');
    Route::get('/furnitures/{furniture}', 'FurnituresController@show');
    Route::patch('/furnitures/{furniture}', 'FurnituresController@update');
    Route::delete('/furnitures/{furniture}', 'FurnituresController@destroy');
});