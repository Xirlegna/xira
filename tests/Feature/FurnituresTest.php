<?php

use App\Furniture;
use App\User;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FurnituresTest extends TestCase {

    use RefreshDatabase;

    protected $user;

    protected function setUp(): void {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    /** @test */
    public function an_unauthenticated_user_should_redirected_to_login() {
        $response = $this->post('/api/furnitures', array_merge($this->data(), ['api_token' => '']));

        $response->assertRedirect('/login');
        $this->assertCount(0, Furniture::all());
    }

    /** @test */
    public function a_furniture_can_be_added() {
        $response = $this->post('/api/furnitures', $this->data());

        $furniture = Furniture::first();

        $this->assertEquals('test_furniture', $furniture->slug_name);
        $this->assertEquals('Test furniture', $furniture->name);
        $this->assertEquals(99999, $furniture->price);
        $this->assertEquals('U alakú kanapé', $furniture->desc);
        $this->assertEquals(90, $furniture->long);
        $this->assertEquals(40, $furniture->height);
        $this->assertEquals(60, $furniture->width);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson(['data' => ['furniture_id' => $furniture->id], 'links' => ['self' => $furniture->path()]]);
    }

    /** @test */
    public function fields_are_required() {
        collect(['slug_name', 'name', 'price'])->each(function($field) {
            $response = $this->post('/api/furnitures', array_merge($this->data(), [$field => '']));

            $response->assertSessionHasErrors($field);
            $this->assertCount(0, Furniture::all());
        });
    }

    /** @test */
    public function fields_are_integer() {
        collect(['price', 'long', 'height', 'width'])->each(function($field) {
            $response = $this->post('/api/furnitures', array_merge($this->data(), [$field => 'abc']));

            $response->assertSessionHasErrors($field);
            $this->assertCount(0, Furniture::all());
        });
    }

    /** @test */
    public function a_furniture_can_be_retrieved() {
        $furniture = factory(Furniture::class)->create();

        $response = $this->get('/api/furnitures/' . $furniture->id . '?api_token=' . $this->user->api_token);

        $response->assertJson([
            'data' => [
                'slug_name' => $furniture->slug_name,
                'name' => $furniture->name,
                'price' => $furniture->price,
                'desc' => $furniture->desc,
                'long' => $furniture->long,
                'height' => $furniture->height,
                'width' => $furniture->width,
            ]
        ]);
    }

    /** @test */
    public function a_furniture_can_be_patched() {
        $furniture = factory(Furniture::class)->create();

        $response = $this->patch('/api/furnitures/' . $furniture->id, $this->data());

        $furniture = $furniture->fresh();

        $this->assertEquals('test_furniture', $furniture->slug_name);
        $this->assertEquals('Test furniture', $furniture->name);
        $this->assertEquals(99999, $furniture->price);
        $this->assertEquals('U alakú kanapé', $furniture->desc);
        $this->assertEquals(90, $furniture->long);
        $this->assertEquals(40, $furniture->height);
        $this->assertEquals(60, $furniture->width);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(['data' => ['furniture_id' => $furniture->id], 'links' => ['self' => $furniture->path()]]);
    }

    /** @test */
    public function a_furniture_can_be_deleted() {
        $furniture = factory(Furniture::class)->create();

        $response = $this->delete('/api/furnitures/' . $furniture->id . '?api_token=' . $this->user->api_token);

        $this->assertCount(0, Furniture::all());
        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

    private function data() {
        return [
            'slug_name' => 'test_furniture',
            'name' => 'Test furniture',
            'price' => 99999,
            'desc' => 'U alakú kanapé',
            'long' => 90,
            'height' => 40,
            'width' => 60,
            'api_token' => $this->user->api_token
        ];
    }

}
