<?php

namespace Tests\Feature;

use App\File;
use App\Furniture;
use App\User;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FilesTest extends TestCase {

    use RefreshDatabase;

    protected $user;
    protected $furniture;

    protected function setUp(): void {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->furniture = factory(Furniture::class)->create();
    }

    /** @test */
    public function a_list_of_files_can_be_fetched_for_the_belong_product() {
        $furniture = factory(Furniture::class)->create();
        $anotherFurniture = factory(Furniture::class)->create();
        
        $file = factory(File::class)->create(['product_id' => $furniture->id]);
        $anotherFile = factory(File::class)->create(['product_id' => $anotherFurniture->id]);

        $response = $this->get('/api/files/furniture/' . $furniture->id . '?api_token=' . $this->user->api_token);
        $response->assertJsonCount(1)->assertJson(['data' => [['data' => ['file_id' => $file->id]]]]);
    }

    /** @test */
    public function an_unauthenticated_user_should_redirected_to_login() {
        $response = $this->post('/api/files', array_merge($this->data(), ['api_token' => '']));

        $response->assertRedirect('/login');
        $this->assertCount(0, File::all());
    }

    /** @test */
    public function a_file_can_be_added() {
        $response = $this->post('/api/files', $this->data());

        $file = File::first();

        $this->assertEquals($this->furniture->id, $file->product_id);
        $this->assertEquals('furnitures', $file->product);
        $this->assertEquals('test_img', $file->name);
        $this->assertEquals('jpg', $file->type);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson(['data' => ['file_id' => $file->id], 'links' => ['self' => $file->path()]]);
    }

    /** @test */
    public function fields_are_required_file() {
        collect(['product', 'name', 'type'])->each(function($field) {
            $response = $this->post('/api/files', array_merge($this->data(), [$field => '']));

            $response->assertSessionHasErrors($field);
            $this->assertCount(0, File::all());
        });
    }

    /** @test */
    public function a_file_can_be_retrieved() {
        $file = factory(File::class)->create();

        $response = $this->get('/api/files/' . $file->id . '?api_token=' . $this->user->api_token);

        $response->assertJson([
            'data'  => [
                'product' => $file->product,
                'name' => $file->name,
                'type' => $file->type,
            ]
        ]);
    }

    /** @test */
    public function a_file_can_be_patched() {
        $file = factory(File::class)->create();

        $response = $this->patch('/api/files/' . $file->id, $this->data());

        $file = $file->fresh();

        $this->assertEquals('furnitures', $file->product);
        $this->assertEquals('test_img', $file->name);
        $this->assertEquals('jpg', $file->type);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(['data' => ['file_id' => $file->id], 'links' => ['self' => $file->path()]]);
    }

    /** @test */
    public function a_file_can_be_deleted() {
        $file = factory(File::class)->create();

        $response = $this->delete('/api/files/' . $file->id . '?api_token=' . $this->user->api_token);

        $this->assertCount(0, File::all());
        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

    private function data() {
        return [
            'product_id' => $this->furniture->id,
            'product' => 'furnitures',
            'name' => 'test_img',
            'type' => 'jpg',
            'api_token' => $this->user->api_token
        ];
    }

}
