<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Furniture extends JsonResource {

    public function toArray($request) {
        return [
            'data' => [
                'furniture_id' => $this->id,
                'slug_name' => $this->slug_name,
                'name' => $this->name,
                'price' => $this->price,
                'desc' => $this->desc,
                'long' => $this->long,
                'height' => $this->height,
                'width' => $this->width,
            ],
            'links' => [
                'self' => $this->path()
            ]
        ];
    }

}
