<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Contact extends JsonResource {

    public function toArray($request) {
        return [
            'data' => [
                'contact_id' => $this->id,
                'name' => $this->name,
                'email' => $this->email,
                'birthday' => $this->birthday->format('Y-m-d'),
                'admin' => $this->admin
            ],
            'links' => [
                'self' => $this->path()
            ]
        ];
    }

}
