<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class File extends JsonResource {

    public function toArray($request) {
        return [
            'data' => [
                'file_id' => $this->id,
                'product_id' => $this->product_id,
                'product' => $this->product,
                'name' => $this->name,
                'type' => $this->type,
            ],
            'links' => [
                'self' => $this->path()
            ]
        ];
    }

}
