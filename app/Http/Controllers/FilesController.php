<?php

namespace App\Http\Controllers;

use App\File;
use App\Furniture;
use App\Http\Resources\File as FileResource;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FilesController extends Controller {

    public function indexFurniture(Furniture $furniture) {
        return FileResource::collection(File::where('product_id', $furniture->id)->where('product', 'furnitures')->get());
    }

    public function store() {
        $file = File::create($this->validateData());

        return (new FileResource($file))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(File $file) {
        return new FileResource($file);
    }

    public function update(File $file) {
        $file->update($this->validateData());

        return (new FileResource($file))->response()->setStatusCode(Response::HTTP_OK);
    }

    public function destroy(File $file) {
        $path = $this->getFilePath($file);
        $imageName = $file->name . '.' . $file->type;
        unlink($path . '/' . $imageName);
        
        if ($this->isEmptyDir($path)) {
            rmdir($path);
        }

        $file->delete();

        return response([], Response::HTTP_NO_CONTENT);
    }

    public function upload(Request $request) {
        $path = 'images/' . $request->product . '/' . $request->product_id;
        $imageName = $request->name . '.' . $request->type;
        $request->image->move(public_path($path), $imageName);

        return response()->json(['success' => 'Jesz']);
    }

    private function getFilePath(File $file) {
        return public_path('images/' . $file->product . '/' . $file->product_id);
    }

    public function isEmptyDir($dir) {
        if (!is_readable($dir)) {
            return null;
        }

        return (count(scandir($dir)) == 2);
    }

    private function validateData() {
        return request()->validate([
                    'product_id' => 'required',
                    'product' => 'required',
                    'name' => 'required',
                    'type' => 'required'
        ]);
    }

}
