<?php

namespace App\Http\Controllers;

use App\Furniture;
use App\Http\Resources\Furniture as FurnitureResource;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FurnituresController extends Controller {

    public function index() {
        return FurnitureResource::collection(Furniture::all());
    }

    public function store() {
        $furniture = Furniture::create($this->validateData());

        return (new FurnitureResource($furniture))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Furniture $furniture) {
        return new FurnitureResource($furniture);
    }

    public function update(Furniture $furniture) {
        $furniture->update($this->validateData());

        return (new FurnitureResource($furniture))->response()->setStatusCode(Response::HTTP_OK);
    }

    public function destroy(Furniture $furniture) {
        $furniture->delete();

        return response([], Response::HTTP_NO_CONTENT);
    }

    private function validateData() {
        return request()->validate([
                    'slug_name' => 'required',
                    'name' => 'required',
                    'price' => 'required|integer',
                    'desc' => '',
                    'long' => 'nullable|integer',
                    'height' => 'nullable|integer',
                    'width' => 'nullable|integer',
        ]);
    }

}
