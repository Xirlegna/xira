<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model {

    protected $table = 'files';
    protected $guarded = [];

    public function path() {
        return '/xira/files/' . $this->id;
    }

    public function furniture() {
        return $this->belongsTo(Furniture::class);
    }

}
