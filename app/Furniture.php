<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Furniture extends Model {

    protected $table = 'furnitures';
    protected $guarded = [];

    public function path() {
        return '/xira/furnitures/' . $this->id;
    }

    public function files() {
        return $this->hasMany(File::class);
    }

}
